# H.I.R.P
*(Honey, I resized the poster)* - Luuse 2018

## Introduction

HIRP is a web structure for printing differents formats of the same content.

## Requierements

 * php
 * SASS-compiler

## Library javascript

 * [jquery](https://jquery.com/) - cross-platform JavaScript library
 * [polyfill.js](https://philipwalton.github.io/polyfill/) - A library to simulate CSS-regions polyfill

## Usage

### Run php

- Go to your command line tool and type `cd to/the/repo/directory`  , then run  `php -S http://localhost:8888/`
- In your browser (preferably Chrome or Chromium), go to http://localhost:8888/.

### Load page

Add hash to url `#A3-1` where:

- `A3` = Print format (as defined in *files/* directory)
- `1` = preview scale

Exemple : http://localhost:8888/index.php#A3-1

### Tree files

```php
├── content
│   └── content.html # content common to all formats
├── files
│   └── A3 
│       ├── index.php # specific structure for A3 
│       ├── style.css 
│       └── style.sass # specific style for A3
│   └── A2 
│       ├── index.php # specific structure for A2 
│       ├── style.css 
│       └── style.sass # specific style for A2
├── img
│   ├── cherie-j-ai-retrecie-les-gosses.jpg
│   └── screens 
│       ├── 0.png
│       ├── 1.png 
│       └── 2.png
│       ├── 3.png
│       ├── 4.png 
│       └── 5.png
├── export
│   ├── Hirp-a2.pdf
│   └── Hirp-a3.pdf
│   └── Hirp-a3.pdf
├── index.php # GUI 
├── js
│   ├── functions.js
│   ├── jquery-3.2.1.min.js
│   ├── main.js
│   └── polyfill.js
├── README.md
├── style
│   ├── env.scss # envirronement style. 
│   ├── fonts.scss
│   ├── hints.scss
│   ├── reset.scss
│   ├── style.scss # global style common to all formats
│   └── variablesGlobal.scss 

```

### Formats

#### Adding formats

```
├── files
    └── A3 
        └── style.scss
```

In specific `style.less` add on the top variables and imports.

```scss
$page-width: 297mm; 
$page-height: 420mm;
$page-bleed: 5mm;
$margin-interior: 5mm;
$margin-outside: 5mm;
$margin-top: 5mm;
$margin-bottom: 5mm;
$u: $page-width / 50;

@import '../../style/env.scss';
@import '../../style/style.scss';
```

#### Change style directory

```
├── files
    └── A3 
        ├── index.php
```

In specific path `index.php` change `href` directory.

```html
<link rel="stylesheet" type="text/css" href="files/A3/style.css?v<?php rand(); ?>" media="all">
```

### Flow

To modify or add content, open and edit `content/content.html` to load it to the defined structure in `files/A3/index.php`.
With `polyfill.js` we can use [css-region](k).

`content.html` is call in `index.php` 

```html
<div id="content" data-content="content/content.html?v<?php echo rand(); ?>" ></div>
```

##### Flow-into / Flow-from

```scss
// extract the <h1/> content to content/content.html
#content h1 {
    flow-into: flow-title;
}

// load <h1/> content trought this class name.
.flow-title {
    flow-from: flow-title;
}
```

## Licence

HIRP is under [GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.pdf)

