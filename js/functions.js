
function readhash(){
  var hash = location.hash.substr(1).split("-");
  return hash;
}

function writehash(docVal, zoomVal) {
  location.hash = '#'+docVal+'-'+zoomVal;
}

function zoomPages(range, content){
  var inputOnLoad = range.val();
  var hash = readhash();
  content.css({'transform': 'scale('+hash[1]+')'});
  document.addEventListener('input', function(e) {
    var value = e.target.value/10;
    if(e.target.className == 'rangeValue'){
      content.css({'transform': 'scale('+value+')'});
      writehash(hash[0], value);
    }
  })
}

function loadFiles(version){
  var hash = readhash();
  var pathContent = $('#content').attr('data-content');
  $('#content').load(pathContent);
  var rdm = Math.random();
  $('.content').load('files/'+ hash[0] +'/index.php#'+ rdm, function(){
    zoomPages($('.rangeValue'), $('.page'));
  })
}

function layout(){
 var allLayout = $("[class^⁼'layout-']");
}
